const main = async () => {
  // Nothing here yet...
  console.log('This is demo scraper implemented in Node.js by Gordan Nekić');
};

// Run it!
main()
  .catch((error) => {
    console.log(`Main system error: ${error}`);
  });
