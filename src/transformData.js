// Load environment variable from .env file if not supplied
const path = require('path');
const _ = require('lodash');
require('dotenv').config({
  path: (typeof process.env.DOTENV_PATH !== 'undefined') ?
    path.resolve(process.cwd(), process.env.DOTENV_PATH) :
    path.resolve(process.cwd(), '.env'),
});

const fs = require('fs');

require('colors');

const fsPromises = fs.promises;

const cheerio = require('cheerio');

const delay = require('./misc/delay.js');

const motherboardsRequests = require('./../resources/motherboards_requests_all_pages.json');

const START_FROM = 1;
const FILE_LOCATION = process.cwd();

const fileName = 'all_items_from_html.json';

const main = async () => {
  console.log(`Total length of dataset: ${motherboardsRequests.length}`);

  const items = [];

  // Loop
  for (let i = START_FROM; i < motherboardsRequests.length; i++) {
    const singleEntry = motherboardsRequests[i];
    console.log(`${i}# Downloading`);

    const html = _.get(singleEntry, 'html');
    const itemMetaData = _.get(singleEntry, 'data');
    const $ = cheerio.load(
      `
      <table>
        <tbody>
          ${html}
        </tbody>
      </table>
      `
    ); // Cheerio and jsdoom have same problem, you need to add <tr tags inside table and tbody or they won't be parsed :S even in browsers, WUT ?!

    console.log('Content length:', html.length);

    $("tr").each((index, element) => {
      const myElem = $(element);

      const itemId = myElem.data('pb-id');

      const itemMetaObj = _.find(itemMetaData, (item) => {
        return item.id === itemId;
      })

      // Name
      // Spec-1
      // Spec-2
      // Spec-3
      // Spec-4
      // Spec-5
      // Rating
      // Price

      const $$ = cheerio.load(
        `
        <table>
          <tbody>
            ${myElem.html()}
          </tbody>
        </table>
        `
      );

      const name = $$('p').text();
      const spec1 = $$('.td__spec--1').text(); // Socket / CPU
      const spec2 = $$('.td__spec--2').text(); // Form Factor
      const spec3 = $$('.td__spec--3').text(); // Memory Max
      const spec4 = $$('.td__spec--4').text(); // Memory Slots
      const spec5 = $$('.td__spec--5').text(); // Color
      const rating = $$('div.td__rating').text();
      const price = $$('.td__price').text();

      items.push({
        id: `${itemId}`,
        imageUrl: itemMetaObj.img,
        name,

        'socket': `${spec1.replace('Socket / CPU', '').trim()}`,
        'formFactor': `${spec2.replace('Form Factor', '').trim()}`,
        'memoryMax': `${spec3.replace('Memory Max', '').trim()}`,
        'memorySlots': `${spec4.replace('Memory Slots', '').trim()}`,
        'color': `${spec5.replace('Color', '').trim()}`,
        'price': `${price.replace('Add', '').trim()}`,
        
        // spec1,
        // spec2,
        // spec3,
        // spec4,
        // spec5,
        // price,
        rating: rating.trim(),
        data: {
          ...itemMetaObj,
        },
        rowText: myElem.text(),
      });
    })

    console.log(items.length);

    console.log('---');

    // eslint-disable-next-line no-await-in-loop
    await delay(1000);
  }

  await fsPromises.writeFile(path.resolve(process.cwd(), `./tmp/${fileName}`), JSON.stringify(items, null, 2), 'utf-8')
    .then(() => {
      console.log(`[file-write] ${path.resolve(process.cwd(), `./tmp/${fileName}`)}`);
      return true;
    })
    .catch((error) => {
      console.log(`[file-write] ERROR: ${JSON.stringify(error, null, 2)}`);
      return false;
    });
};

// Run it!
main()
  .catch((error) => {
    console.log(`Main system error: ${error}`);
  });
