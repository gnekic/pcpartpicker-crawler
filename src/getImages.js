// Load environment variable from .env file if not supplied
const path = require('path');
const _ = require('lodash');
require('dotenv').config({
  path: (typeof process.env.DOTENV_PATH !== 'undefined') ?
    path.resolve(process.cwd(), process.env.DOTENV_PATH) :
    path.resolve(process.cwd(), '.env'),
});

const fs = require('fs');

require('colors');

const fsPromises = fs.promises;

const axios = require('axios');

const delay = require('./misc/delay.js');

const shopItems = require('./../resources/all_items_from_html.json');

const START_FROM = 1;
const FILE_LOCATION = process.cwd();

const PARAM_TO_USE = 'imageUrl';
const FOLDER = 'images'; // Original
const FOLDER_2 = 'images-custom-name'; // Only id + original

const main = async () => {
  console.log(`Total length of dataset: ${shopItems.length}`);

  const items = [];

  // Loop
  for (let i = START_FROM; i < shopItems.length; i++) {
    const singleEntry = shopItems[i];
    console.log(`${i}# Downloading`);

    const id = _.get(singleEntry, 'id');

    const pictureUrl = _.get(singleEntry, PARAM_TO_USE);
    if (typeof pictureUrl === 'string') {
      console.log('|- Picture exists in dataset...'.bgGreen.black);

      const id = _.get(singleEntry, 'id');

      const matchResult = pictureUrl.match(/^(.+)\/(.+?)$/);

      if (typeof _.get(matchResult, '[2]') !== 'undefined') {
        const fileName = matchResult[2];

        console.log(`|- Filename: ${fileName}`);
        // eslint-disable-next-line no-await-in-loop
        const imageResponse = await axios({
          method: 'get',
          url: (pictureUrl.substr(0, 2) === '//') ? `https:${pictureUrl}` : pictureUrl,
          responseType: 'arraybuffer',
        })
          .then((res) => {
            return Buffer.from(res.data, 'binary');
          })
          .catch((error) => {
            if (_.get(error, 'response.status') === 403) {
              console.log('|- NO ACCESS TO IMAGE, STATUS: 403'.bgRed.black);
            } else {
              console.log(`[axios] ERROR: ${JSON.stringify(error, null, 2)}`.bgRed.black);
            }
          });

        if (typeof imageResponse !== 'undefined') {
          // eslint-disable-next-line no-await-in-loop
          await fsPromises.writeFile(path.resolve(FILE_LOCATION, `./tmp/${FOLDER}/${fileName}`), imageResponse)
            .then(() => {
              console.log(`[file-write] ${`./tmp/${FOLDER}/${fileName}`}`.bgGreen.black);
              // console.log(`[file-write] ${path.resolve(FILE_LOCATION, `./tmp/${FOLDER}/${id}_---_${fileName}`)}`);
              return true;
            })
            .catch((error) => {
              console.log(`[file-write] ERROR: ${JSON.stringify(error, null, 2)}`.bgRed.black);
              return false;
            });

          await fsPromises.writeFile(path.resolve(FILE_LOCATION, `./tmp/${FOLDER_2}/${id}_---_${fileName}`), imageResponse)
            .then(() => {
              console.log(`[file-write] ${`./tmp/${FOLDER}/${id}_---_${fileName}`}`.bgGreen.black);
              // console.log(`[file-write] ${path.resolve(FILE_LOCATION, `./tmp/${FOLDER}/${id}_---_${fileName}`)}`);
              return true;
            })
            .catch((error) => {
              console.log(`[file-write] ERROR: ${JSON.stringify(error, null, 2)}`.bgRed.black);
              return false;
            });
        }
      }
    } else {
      console.log('|- Picture DOES NOT exists in dataset...'.bgMagenta.black);
    }

    console.log('---');

    // eslint-disable-next-line no-await-in-loop
    await delay(100);
  }
};

// Run it!
main()
  .catch((error) => {
    console.log(`Main system error: ${error}`);
  });
