// Load environment variable from .env file if not supplied
const path = require('path');
const _ = require('lodash');
require('dotenv').config({
  path: (typeof process.env.DOTENV_PATH !== 'undefined') ?
    path.resolve(process.cwd(), process.env.DOTENV_PATH) :
    path.resolve(process.cwd(), '.env'),
});

const fs = require('fs');

const fsPromises = fs.promises;

require('colors');

const axios = require('axios');

const delay = require('./misc/delay.js');

const START_FROM = 1;
const STOP_FROM = 32; // 32 pages
const fileName = 'motherboards.json';

const main = async () => {
  console.log('PLEASE BEFORE YOU RUN THIS SCRIPT, Know what you are doing, edit headers accordingly or else you will get insta IP ban'); // REMOVE ME!
  return; // REMOVE ME!
  const headers = {
    'authority': 'pcpartpicker.com',
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'x-csrftoken': 'fyRZeLltnVAz4eSjcGVpsoDZfze2Vy20douaweuowdmcGtSm3SNbEM3QW9n4siAK',
    'x-requested-with': 'XMLHttpRequest',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.142 Safari/537.36',
    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'origin': 'https://pcpartpicker.com',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'referer': 'https://pcpartpicker.com/products/motherboard/',
    'accept-language': 'en-US,en;q=0.9',
    'cookie': '__cfduid=d6ac3e4cb993c47e5c5f35abcefa17c111590256509; xcsrftoken=fyRZeLltnVAz4eSjcGVpsoDZfze2Vy20douaweuowdmcGtSm3SNbEM3QW9n4siAK; xsessionid=nxch5nmnkg2zk2fi0tcgmyk72cxsg4rd; xgdpr-consent=allow; _ga=GA1.2.1336271127.1590256552; _gid=GA1.2.1605733959.1590256552'
  };

  const resultFullData = [];
  // Loop
  for (let i = START_FROM; i < STOP_FROM + 1; i++) {
    /*
    --data-raw 'page=3&xslug=&location=&token=&search=&qid=2&scr=1&scr_vw=858&scr_vh=956&scr_dw=1920&scr_dh=1080&scr_daw=1920&scr_dah=1040&scr_ddw=858&scr_ddh=13489&ms=1590249273161'
    */

   var datetime = +(new Date()) // Not needed 

   const dataString = `page=${i}&xslug=&location=&token=78352e762c8b458a88b494e6e6ce2749%3AfxXzRpVIymG%2BbRpMWl2qQ%2BLDqoSStIZOyQNzzafqYt4QT3XbR%2B5hbEWx3yZ3PGI%2BqfdpuRgd%2FQWoZoLSrHeb8g%3D%3D&search=&qid=${i}&scr=1&scr_vw=428&scr_vh=801&scr_dw=1368&scr_dh=912&scr_daw=1368&scr_dah=872&scr_ddw=428&scr_ddh=14129&ms=${datetime}`;

    // eslint-disable-next-line no-await-in-loop
    const response = await axios({
      method: 'POST',
      url: 'https://pcpartpicker.com/products/motherboard/fetch/',
      // url: 'https://postman-echo.com/post',
      headers,
      data: dataString, // Ovo je divlje, u axios-u je data, u requestu je body, insta IP ban ako je body prazan (Prvi trik)
    })
      .then((res) => {
        return res;
      })
      .catch((error) => {
        console.error(error);
      });

    console.log(`Request (${i}):`);
    // console.log(_.get(response, 'data.result'));

    if (typeof _.get(response, 'data.result') !== 'undefined') {
      const itemsResult = _.get(response, 'data.result');
      console.log('Length of types:', itemsResult.data.length);
      resultFullData.push({
        page: i,
        ...itemsResult,
      });
    }

    console.log('---');
    console.log('Gathered:', resultFullData.length);
    console.log('---');

    // eslint-disable-next-line no-await-in-loop
    await delay(1000);
  }

  await fsPromises.writeFile(path.resolve(process.cwd(), `./tmp/${fileName}`), JSON.stringify(resultFullData, null, 2), 'utf-8')
    .then(() => {
      console.log(`[file-write] ${path.resolve(process.cwd(), `./tmp/${fileName}`)}`);
      return true;
    })
    .catch((error) => {
      console.log(`[file-write] ERROR: ${JSON.stringify(error, null, 2)}`);
      return false;
    });

  // eslint-disable-next-line no-await-in-loop
  await delay(100);
};

// Run it!
main()
  .catch((error) => {
    console.log(`Main system error: ${error}`);
  });
